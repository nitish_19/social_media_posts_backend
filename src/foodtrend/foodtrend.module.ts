import { Module } from '@nestjs/common';
import { FoodtrendService } from './foodtrend.service';
import { FoodtrendController } from './foodtrend.controller';
import { FoodTrendSchema } from './schema/foodtrend.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'FoodTrend', schema: FoodTrendSchema }]),
  ],
  providers: [FoodtrendService],
  controllers: [FoodtrendController],
})
export class FoodtrendModule {}
