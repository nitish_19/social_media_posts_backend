import { Test, TestingModule } from '@nestjs/testing';
import { FoodtrendService } from './foodtrend.service';

describe('FoodtrendService', () => {
  let service: FoodtrendService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FoodtrendService],
    }).compile();

    service = module.get<FoodtrendService>(FoodtrendService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
