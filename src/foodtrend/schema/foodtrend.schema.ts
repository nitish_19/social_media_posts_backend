import * as mongoose from 'mongoose';
const { Schema } = mongoose;

export const FoodTrendSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
});
