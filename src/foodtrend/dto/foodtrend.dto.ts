import { IsNotEmpty } from 'class-validator';

export class FoodTrendDto {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  image: string;
}
