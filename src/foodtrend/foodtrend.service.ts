import { Injectable } from '@nestjs/common';
import { FoodTrend } from './interface/foodtrend.interface';
import { InjectModel } from '@nestjs/mongoose';
import { FoodTrendDto } from './dto/foodtrend.dto';
import { Model } from 'mongoose';

@Injectable()
export class FoodtrendService {
  constructor(
    @InjectModel('FoodTrend') private readonly foodTrendModel: Model<FoodTrend>,
  ) {}

  // Food Trend creation service
  async create(foodTrendDto: FoodTrendDto) {
    const foodTrend = new this.foodTrendModel(foodTrendDto);
    return await foodTrend.save();
  }

  // Retrieving all food trends service
  async getAllFoodTrends(): Promise<FoodTrend[]> {
    const foodTrends: FoodTrend[] = await this.foodTrendModel.find();
    return foodTrends;
  }
}
