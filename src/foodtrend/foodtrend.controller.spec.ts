import { Test, TestingModule } from '@nestjs/testing';
import { FoodtrendController } from './foodtrend.controller';

describe('FoodtrendController', () => {
  let controller: FoodtrendController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FoodtrendController],
    }).compile();

    controller = module.get<FoodtrendController>(FoodtrendController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
