import {
  Controller,
  Post,
  Body,
  Res,
  Get,
  Param,
  HttpStatus,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FoodTrendDto } from './dto/foodtrend.dto';
import { FoodTrend } from './interface/foodtrend.interface';
import { FoodtrendService } from './foodtrend.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Express } from 'express';
import {
  imageFileName,
  profileTypeFilter,
} from './utils/foodtrend-image.utils';

@Controller('foodtrend')
export class FoodtrendController {
  constructor(private foodTrendService: FoodtrendService) {}

  @Post('create')
  async create(@Res() res, @Body() foodTrendDto: FoodTrendDto) {
    try {
      const foodTrend: FoodTrend = await this.foodTrendService.create(
        foodTrendDto,
      );

      return res.status(HttpStatus.CREATED).json({
        status: 201,
        message: 'success',
        data: foodTrend,
      });
    } catch (err) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        status: err.status,
        message: err.message,
      });
    }
  }

  // API for user Food Trend image Upload
  @Post('image-upload')
  @UseInterceptors(
    FileInterceptor('image', {
      storage: diskStorage({
        destination: './images',
        filename: imageFileName,
      }),
      fileFilter: profileTypeFilter,
    }),
  )
  async addFoodTrendImage(
    @Res() res,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return res.status(HttpStatus.CREATED).json({
      status: 201,
      message: 'success',
      data: file.originalname,
    });
  }

  // API for retrieving all food trends
  @Get('all')
  async getAllFoodTrends(@Res() res) {
    try {
      const foodTrends: FoodTrend[] =
        await this.foodTrendService.getAllFoodTrends();
      return res.status(HttpStatus.CREATED).json({
        status: 201,
        message: 'success',
        data: foodTrends,
      });
    } catch (err) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        status: err.status,
        message: err.message,
      });
    }
  }

  @Get('image/:filename')
  getProfileImage(@Param('filename') image, @Res() res) {
    return res.sendFile(image, { root: './images' });
  }
}
