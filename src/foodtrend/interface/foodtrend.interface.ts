import { Document } from 'mongoose';

export interface FoodTrend extends Document {
  readonly title: string;
  readonly description: string;
  readonly image: string;
}
